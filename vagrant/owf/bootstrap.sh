#!/usr/bin/env bash

echo "America/New_York" | sudo tee /etc/timezone
dpkg-reconfigure --frontend noninteractive tzdata

apt-get update
apt-get install -y tomcat7 tomcat7-docs tomcat7-admin tomcat7-examples default-jdk git subversion maven postgresql-9.3 postgresql-contrib-9.3 postgresql-9.3-postgis-2.1

service tomcat7 stop

rm -f /etc/tomcat7/tomcat-users.xml
rm -f /etc/tomcat7/server.xml
rm -f /etc/ssl/certs/java/cacerts

CATALINA_HOME=/var/lib/tomcat7

ln -s /vagrant/config/tomcat-users.xml /etc/tomcat7/tomcat-users.xml
ln -s /vagrant/config/server.xml /etc/tomcat7/server.xml
ln -s /vagrant/config/certs $CATALINA_HOME/certs
ln -s /vagrant/config/certs/keystore.jks /etc/ssl/certs/java/cacerts

cp /vagrant/config/pg_hba.conf /etc/postgresql/9.3/main
cp /vagrant/config/postgresql.conf /etc/postgresql/9.3/main

service postgresql restart

sudo -u postgres psql -f /vagrant/config/owf.sql
sudo -u postgres psql -f /vagrant/config/psql_ext.sql
export PGPASSWORD=owf
psql -U owf_user owf -f /vagrant/config/PostgreSQLPrefsInitialCreate.sql

cp /vagrant/webapps/owf.war $CATALINA_HOME/webapps
cp /vagrant/webapps/cas.war $CATALINA_HOME/webapps

sudo -u tomcat7 unzip -q $CATALINA_HOME/webapps/owf.war -d $CATALINA_HOME/webapps/owf
sudo -u tomcat7 unzip -q $CATALINA_HOME/webapps/cas.war -d $CATALINA_HOME/webapps/cas

CATALINA_LIBS=/usr/share/tomcat7/lib
OWF_CLASSES=$CATALINA_HOME/webapps/owf/WEB-INF/classes

cp /vagrant/config/OwfConfig.groovy $OWF_CLASSES
cp /vagrant/config/OWFsecurityContext.xml $OWF_CLASSES
cp /vagrant/config/owf-override-log4j.xml $OWF_CLASSES
cp /vagrant/config/OzoneConfig.properties $OWF_CLASSES
cp -r /vagrant/config/ozone-security-beans $OWF_CLASSES
cp /vagrant/config/CASSpringOverrideConfig.xml $CATALINA_HOME/webapps/cas/WEB-INF/classes
cp /vagrant/config/cas_log4j.xml $CATALINA_HOME/webapps/cas/WEB-INF/classes
cp /vagrant/config/users.properties $CATALINA_LIBS
cp -r /vagrant/config/js-plugins $CATALINA_LIBS
cp -r /vagrant/config/themes $CATALINA_LIBS
cp /vagrant/config/postgresql-9.3-1103.jdbc41.jar $CATALINA_LIBS

for i in `ls -1 /vagrant/config/conf.d` ;
do
  sh /vagrant/config/conf.d/$i ;
done

service tomcat7 start
